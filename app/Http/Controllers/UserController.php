<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Illuminate\Http\Response;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getAccountDetails(UserService $userService)
    {
        $userDetails = $userService->accountDetails();
        return response()->json(['status'=>"success","data" => $userDetails], 200, []);
    }

    
}
